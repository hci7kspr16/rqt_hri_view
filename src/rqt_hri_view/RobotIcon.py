from python_qt_binding.QtGui import *
from python_qt_binding.QtCore import *
 
class RobotIcon(QLabel):
 
    def __init__(self, text, parent):
        QLabel.__init__(self, text, parent)
        
    def __init__(self, text):
        QLabel.__init__(self, text)
        
           
    def mouseReleaseEvent(self, ev):
        self.emit(SIGNAL('clicked()'))

class RobotWidget(QGraphicsSimpleTextItem):
    def __init__(self, text, robotColor):
        QGraphicsSimpleTextItem.__init__(self, text)
        self._selected = False
        self._color = QColor(robotColor)
        self._color.setAlpha(100)
        
    def selected(self, m_sel):
        self._selected = m_sel
        
    def mouseReleaseEvent(self, ev):
        self.emit(SIGNAL('clicked()'))
        
    def paint(self, qp, options, widget):
        if self._selected:
            qp.setBrush(QBrush(self._color)) #light transparent version of the text color
            qp.drawRect(0,1,2,2)
            
        QGraphicsSimpleTextItem.paint(self,qp, options, widget)
