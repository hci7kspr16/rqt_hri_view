# Software License Agreement (BSD License)
#
# Copyright (c) 2012, Willow Garage, Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of Willow Garage, Inc. nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import rospy
import tf
from tf.transformations import quaternion_from_euler

import numpy
import random
from math import sqrt, atan, pi, degrees, floor
from sensor_msgs.msg import Image
from nav_msgs.msg import OccupancyGrid, Path, Odometry
from geometry_msgs.msg import PolygonStamped, PointStamped, PoseWithCovarianceStamped, PoseStamped

from python_qt_binding.QtCore import Signal, Slot, QPointF, qWarning, Qt, SIGNAL
from python_qt_binding.QtCore import *
from python_qt_binding.QtGui import QWidget, QPixmap, QImage, QGraphicsView, QGraphicsScene, QPainterPath, QPen, QPolygonF, QVBoxLayout, QHBoxLayout, QColor, qRgb, QPushButton, QLabel, QLineEdit
from python_qt_binding.QtGui import *

from rqt_py_common.topic_helpers import get_field_type

import RobotIcon

def accepted_topic(topic):
    msg_types = [OccupancyGrid, Path, PolygonStamped, PointStamped]
    msg_type, array = get_field_type(topic)

    if not array and msg_type in msg_types:
        return True
    else:
        return False

class HRIViewWidget(QWidget):

    def __init__(self, map_topic='/map'):
        super(HRIViewWidget, self).__init__()
        self._layout = QVBoxLayout()
        self._h_layout = QHBoxLayout()
        self.setAcceptDrops(True)
        self.setWindowTitle('HRI Navigation Viewer')

        self.map = map_topic
        self._tf = tf.TransformListener()

        
        self._robot_labels = (QLabel('Robot 0 Name:'), QLabel('Robot 1 Name:'),
                              QLabel('Robot 2 Name:'), QLabel('Robot 3 Name:'))
        self._robot_nss = (QLineEdit('/r0'), QLineEdit('/r1'), QLineEdit('/r2'), QLineEdit('/r3'))
        self._robotColors = [Qt.red, Qt.blue, Qt.green, Qt.magenta]

        robotLayout = QHBoxLayout()
        for theLabel, theTextBox in zip(self._robot_labels, self._robot_nss):
            robotLayout.addWidget(theLabel)
            robotLayout.addWidget(theTextBox)
        self._layout.addLayout(robotLayout)

        hNavLayout = QHBoxLayout()
        vVidLayout = QVBoxLayout()

        for theIndex in range(0,2):
            theNS = self._robot_nss[theIndex] 
            vVidLayout.addWidget(HRICamView(theIndex, theNS.text(), parent = self))
        hNavLayout.addLayout(vVidLayout)

        self._nav_view = HRIView(map_topic, tf = self._tf, parent = self)

        hNavLayout.addWidget(self._nav_view)
        vVidLayout = QVBoxLayout()
        for theIndex in range(2,4):
            theNS = self._robot_nss[theIndex] 
            vVidLayout.addWidget(HRICamView(theIndex, theNS.text(), parent = self))

        hNavLayout.addLayout(vVidLayout)
        self._layout.addLayout(hNavLayout)
        self.setLayout(self._layout)

        
    def dragEnterEvent(self, e):
        print 'DragEnter, nav'
        if not e.mimeData().hasText():
            if not hasattr(e.source(), 'selectedItems') or len(e.source().selectedItems()) == 0:
                qWarning('HRIView.dragEnterEvent(): not hasattr(event.source(), selectedItems) or len(event.source().selectedItems()) == 0')
                return
            item = e.source().selectedItems()[0]
            topic_name = item.data(0, Qt.UserRole)
            if topic_name == None:
                qWarning('HRIView.dragEnterEvent(): not hasattr(item, ros_topic_name_)')
                return

        else:
            topic_name = str(e.mimeData().text())

        if accepted_topic(topic_name):
            e.accept()
            e.acceptProposedAction()

    def dropEvent(self, e):
        print 'DragDrop, nav'
        if e.mimeData().hasText():
            topic_name = str(e.mimeData().text())
        else:
            droped_item = e.source().selectedItems()[0]
            topic_name = str(droped_item.data(0, Qt.UserRole))

        topic_type, array = get_field_type(topic_name)
        if not array:
            if topic_type is OccupancyGrid:
                self.map = topic_name

                # Swap out the nav view for one with the new topics
                self._nav_view.close()
                self._nav_view = HRIView(self.map, self.paths, self.polygons, self._tf, self)
                self._layout.addWidget(self._nav_view)

    def save_settings(self, plugin_settings, instance_settings):
        self._nav_view.save_settings(plugin_settings, instance_settings)

    def restore_settings(self, plugin_settings, instance_settings):
        self._nav_view.restore_settings(plugin_settings, instance_settings)

class OccupancyMember(object):
    def __init__(self, m_x=0, m_y=0, m_robotID=0):
        self.x = m_x
        self.y = m_y
        self.robotID = m_robotID

class HRICamView(QGraphicsView):
    #Subscribe to a camera view on this topic
    image_changed = Signal()
    def __init__(self, robotID, robotNS, parent=None):
        super(HRICamView,self).__init__()
        self._parent = parent
        self._robotID = robotID
        self._robotNS = robotNS
        self._scene = QGraphicsScene()
        self.setScene(self._scene)
        self._imageSub = rospy.Subscriber(robotNS + '/front_camera/image_raw', Image, self.image_cb)
        self.w = 0
        self.h = 0
        self.image_changed.connect(self._updateImage)
        self._image = None
        self._imageGI = None
        self.setMaximumWidth(400)

        self._robotLabel = self._scene.addSimpleText(str(robotID))
        self._color = QColor(self._parent._robotColors[robotID])
        self._color.setAlpha(100)
        self._robotLabel.setBrush(QBrush(self._color))
        self._robotLabel.setFont(QFont("SansSerif", 100, weight=QFont.Bold))
        self._robotLabel.setZValue(1)
        self._robotLabel.setPos(QPointF(0, 0))
        
    def image_cb(self, msg):
        self._image = QPixmap.fromImage(QImage(msg.data, msg.width, msg.height, QImage.Format_RGB888))
        self.w = msg.width
        self.h = msg.height
        self.image_changed.emit()
        
    def _updateImage(self):
        if self._imageGI:
            self._scene.removeItem(self._imageGI)
         #self._image.height/2)
        self._imageGI = self._scene.addPixmap(self._image)
        self._imageGI.setZValue(0)
        self.setSceneRect(0, 0, self.w, self.h)

    def resizeEvent(self, evt=None):
        #Resize

        bounds = self._scene.itemsBoundingRect()

        if bounds:
            #Wait until bounds exist, which only happen after the map is updated once
            bounds.setWidth(bounds.width()*1.0)         
            bounds.setHeight(bounds.height()*1.0)
            self.fitInView(bounds, Qt.KeepAspectRatio)
            #self.centerOn(self._imageGI)
            self.show()
            
class HRIView(QGraphicsView):
    map_changed = Signal()
    robot_odom_changed = Signal(int)
    map_visited_changed = Signal(OccupancyMember)
    
    def __init__(self, map_topic='/map',
                 tf=None, parent=None):
        super(HRIView, self).__init__()
        self._parent = parent

        self._goal_mode = True

        self.map_changed.connect(self._update)
        self.destroyed.connect(self.close)
        self.robot_odom_changed.connect(self._updateRobots)
        self.map_visited_changed.connect(self._updateVisited)
        

        self.setDragMode(QGraphicsView.NoDrag)

        self._map = None
        self._map_item = None

        self.w = 0
        self.h = 0

        self._colors = [(238, 34, 116), (68, 134, 252), (236, 228, 46), (102, 224, 18), (242, 156, 6), (240, 64, 10), (196, 30, 250)]
        self._scene = QGraphicsScene()

        if tf is None:
            self._tf = tf.TransformListener()
        else:
            self._tf = tf
        self.map_sub = rospy.Subscriber('/map', OccupancyGrid, self.map_cb)

        self.robot_subs = dict()
        self._robotIcons = dict()
        self._robotColors = self._parent._robotColors
        
        self._robotLocations = dict()
        self._robotAdded = dict()
        self._currentRobot = -1;
        self._robotCmdPubs = dict()
        
        for theSub,i in zip(self._parent._robot_nss, range(0,len(self._parent._robot_nss))):
           self.robot_subs[i] = rospy.Subscriber(theSub.text() + '/odom', Odometry, self.robot_odom_cb, i)
           
           #Add to dictionaries for later by robotID
           self._robotLocations[i] = [0,0];
           self._robotAdded[i] = 0;
           self._robotCmdPubs[i] = rospy.Publisher(theSub.text() + '/simple_goal', PoseStamped, queue_size=10)

        self.setScene(self._scene)

    def add_dragdrop(self, item):
        # Add drag and drop functionality to all the items in the view
        def c(x, e):
            self.dragEnterEvent(e)
        def d(x, e):
            self.dropEvent(e)
        item.setAcceptDrops(True)
        item.dragEnterEvent = c
        item.dropEvent = d

    def dragEnterEvent(self, e):
        if self._parent:
            self._parent.dragEnterEvent(e)

    def dropEvent(self, e):
        if self._parent:
            self._parent.dropEvent(e)
            
    def robot_odom_cb(self, msg, robotID):
        #print 'Got odometry from robot ' + str(robotID)


        #Resolve the odometry to a screen coordinate for display

        worldX = msg.pose.pose.position.x
        worldY = msg.pose.pose.position.y

        #print 'Robot at: ' + str(worldX) + ',' + str(worldY)

        self._robotLocations[robotID] = [worldX, worldY]
       
        self.robot_odom_changed.emit(robotID)
        self.map_visited_changed.emit(OccupancyMember(worldX, worldY, robotID))
       
            
    def map_cb(self, msg):
        self.resolution = msg.info.resolution
        self.w = msg.info.width
        self.h = msg.info.height
        self._visited = numpy.empty((self.w, self.h), dtype=OccupancyMember)
        self._visitedFlag = numpy.zeros((self.w, self.h), dtype=numpy.uint8)
        
        a = numpy.array(msg.data, dtype=numpy.uint8, copy=False, order='C')
        a = a.reshape((self.h, self.w))
        if self.w % 4:
            e = numpy.empty((self.h, 4 - self.w % 4), dtype=a.dtype, order='C')
            a = numpy.append(a, e, axis=1)
        image = QImage(a.reshape((a.shape[0] * a.shape[1])), self.w, self.h, QImage.Format_Indexed8)

        for i in reversed(range(101)):
            image.setColor(100 - i, qRgb(i* 2.55, i * 2.55, i * 2.55))
        image.setColor(101, qRgb(255, 0, 0))  # not used indices
        image.setColor(255, qRgb(200, 200, 200))  # color for unknown value -1
        self._map = image
        self.setSceneRect(0, 0, self.w, self.h)

        self.map_changed.emit()

    def goal_mode(self):
        self.setDragMode(QGraphicsView.NoDrag)
  

    def draw_position(self, e, mirror=True):
        p = self.mapToScene(e.x(), e.y())
        v = (p.x() - self.drag_start[0], p.y() - self.drag_start[1])
        mag = sqrt(pow(v[0], 2) + pow(v[1], 2))
        u = (v[0]/mag, v[1]/mag)

        res = (u[0]*20, u[1]*20)
        path = self._scene.addLine(self.drag_start[0], self.drag_start[1],
                                   self.drag_start[0] + res[0], self.drag_start[1] + res[1])

        if self.last_path:
            self._scene.removeItem(self.last_path)
            self.last_path = None
        self.last_path = path

        if mirror:
            # Mirror point over x axis
            x = ((self.w / 2) - self.drag_start[0]) + (self.w /2)
        else:
            x = self.drag_start[0]

        map_p = [x * self.resolution, self.drag_start[1] * self.resolution]

        angle = atan(u[1] / u[0])
        quat = quaternion_from_euler(0, 0, degrees(angle))

        return map_p, quat

    def mousePressEvent(self, e):

        #Top left, growing right and down
        p = self.mapToScene(e.x(), e.y())
        #print 'Mapped coordinates:'
        #print p



        #See if we hit a label (a robot?)
        foundRobot = False
        foundMap = False
        robot_label = None
        theMap = None
        
        pointList = self._scene.items(QPointF(p.x(), p.y()))
        for theObj in pointList:
            if isinstance(theObj, RobotIcon.RobotWidget):
                foundRobot = True
                robot_label = theObj
            elif isinstance(theObj, QGraphicsPixmapItem):
                foundMap = True
                theMap = theObj
        
        if foundRobot:
            print 'Selected robot:' + robot_label.text()
            robotID = int(robot_label.text())
            if self._currentRobot != -1:
                #Reset the old one if selected
                self._robotIcons[self._currentRobot].selected(False)
                
            if self._currentRobot == robotID:
                #Unselect ourselves
                robot_label.selected(False)
                #self._robotIcons[self._currentRobot].setStyleSheet("""background-color: rgba(255, 255, 255, 0);""")
                self._currentRobot = -1
            else:
                #Set the new one
                #robot_label.setStyleSheet("""background-color: rgba(0, 255, 0, 120);""")
                robot_label.selected(True)
                self._currentRobot = robotID
                    
        elif foundMap:
            #They only clicked on the map, if there's a robot selected, send it to the point
            if self._currentRobot != -1:
                print 'Sending robot ' + str(self._currentRobot) + ' to ' + str(p.x()) + ',' + str(self.h - p.y()) 
                msg = PoseStamped()
                msg.header.frame_id = '/map'
                msg.header.stamp = rospy.Time.now()

                msg.pose.position.x = p.x()
                msg.pose.position.y = self.h - p.y()
                msg.pose.orientation.w = 1.0
                msg.pose.orientation.z = 0

                self._robotCmdPubs[self._currentRobot].publish(msg)
            
    def mouseReleaseEvent(self, e):
        if 0:
            self._goal_mode = False
            map_p, quat = self.draw_position(e)

            msg = PoseStamped()
            msg.header.frame_id = '/map'
            msg.header.stamp = rospy.Time.now()

            msg.pose.position.x = map_p[0]
            msg.pose.position.y = map_p[1]
            msg.pose.orientation.w = quat[0]
            msg.pose.orientation.z = quat[3]




    #def mouseMoveEvent(self, e):
    #    if e.buttons() == Qt.LeftButton and (self._pose_mode or self._goal_mode):
    #        map_p, quat = self.draw_position(e)

    def close(self):
        if self.map_sub:
            self.map_sub.unregister()
        super(HRIView, self).close()

    def resizeEvent(self, evt=None):
        #Resize map to fill window

        bounds = self._scene.itemsBoundingRect()
        if bounds:
            #Wait until bounds exist, which only happen after the map is updated once
            bounds.setWidth(bounds.width()*1.0)         
            bounds.setHeight(bounds.height()*1.0)
            self.fitInView(bounds, Qt.KeepAspectRatio)
            self.centerOn(self._map_item)
            self.show()
        
    def robotIconClicked(self, evt):
        print 'Clicked Icon!'
        print evt
        
    def _updateVisited(self, gridLoc):
        #gridLoc is of type OccupancyMember
        gridLoc.x = floor(gridLoc.x)
        gridLoc.y = floor(gridLoc.y)
        #print 'Got a visit of (%d, %d) by robot %d' % (gridLoc.x, gridLoc.y, gridLoc.robotID)
        if self._visitedFlag[gridLoc.x, gridLoc.y] == 1:
            #Update the existing marker to reflect the latest robot to traverse it
            mem = self._visited[gridLoc.x, gridLoc.y]
            if mem.robotID != gridLoc.robotID:
                mem.robotID = gridLoc.robotID
                self._scene.removeItem(mem.marker)
                theColor = QColor(self._robotColors[gridLoc.robotID])
                theColor.setAlpha(80)
                thePen = QPen(theColor)
                thePen.setWidthF(self.h / 20)
                mem.marker = self._scene.addEllipse(gridLoc.x + 0.5, self.h - gridLoc.y - .5, .25, .25, thePen)
        else:
            #Add a new OccupancyMember to the reference, add it to the scenegraph,
            #and set the flag
            mem = OccupancyMember()
            mem.robotID = gridLoc.robotID
            theColor = QColor(self._robotColors[gridLoc.robotID])
            theColor.setAlpha(80)
            thePen = QPen(theColor)
            thePen.setWidthF(self.h / 20)
            mem.marker = self._scene.addEllipse(gridLoc.x + 0.5, self.h - gridLoc.y - .5, .25, .25, thePen)

            self._visited[gridLoc.x, gridLoc.y] = mem
            self._visitedFlag[gridLoc.x, gridLoc.y] = 1
            
    def _updateRobots(self, robotID):
        #Redraw the robot locations
        #print 'Updating robots'
        #If this is the first time we've seen this robot, create its icon
        if self._robotAdded[robotID] == 0:
            thisRobot = RobotIcon.RobotWidget(str(robotID), self._robotColors[robotID])
            thisRobot.setFont(QFont("SansSerif", max(self.h / 20.0,2), QFont.Bold))
            thisRobot.setBrush(QBrush(self._robotColors[robotID]))
          
            self._robotIcons[robotID] = thisRobot

            self._scene.addItem(thisRobot)
   
            self._robotAdded[robotID] = 1
            

        #Pick up the world coordinates
        world = self._robotLocations[robotID]

        iconBounds = self._robotIcons[robotID].boundingRect()
        #Adjust the world coords so that the icon is centered on the robot, rather than top-left
        world[0] = world[0]  - iconBounds.width()/2 + 0.75
        
        world[1] = self.h - (world[1] + iconBounds.height()/2) #mirror the y coord
        self._robotIcons[robotID].setPos(QPointF(world[0], world[1]))
        

        
        
    def _update(self):
        if self._map_item:
            self._scene.removeItem(self._map_item)

        pixmap = QPixmap.fromImage(self._map)
        self._map_item = self._scene.addPixmap(pixmap)

        # Everything must be mirrored
        self._mirror(self._map_item)

        # Add drag and drop functionality
        self.add_dragdrop(self._map_item)

        #Resize map to fill window
        bounds = self._scene.itemsBoundingRect()
       
        bounds.setWidth(bounds.width()*1.0)         
        bounds.setHeight(bounds.height()*1.0)
        self.fitInView(bounds, Qt.KeepAspectRatio)
        self.centerOn(self._map_item)
        self.show()

    def _mirror(self, item):
        item.scale(-1, 1)
        item.translate(-self.w, 0)

    def save_settings(self, plugin_settings, instance_settings):
        # TODO add any settings to be saved
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO add any settings to be restored
        pass
